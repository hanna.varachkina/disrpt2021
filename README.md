1. Open this collaboratory notebook  
[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/14RL3BAG4sKGmZFpx2TkdEnEOgqm3Futs?usp=sharing)

2. In the drop-down menu of the 4th cell choose a language from the list

3. Run all cells

4. Save the downloaded file and run the evaluation sript
